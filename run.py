
import RPi.GPIO as GPIO
import pygame
import time

TIME_CLOSED = 75 # seconds
WAIT_AFTER_RESET = 2

PLAYER_BUTTON_PIN = 11
OPERATOR_OVEN_OPEN_CLOSE_PIN = 12
OPERATOR_ALLOW_START_PIN = 18

OVEN_OPEN_PIN = 15
OVEN_START_ALLOWED_OR_ON_PIN = 36

MAGNET_LOCK_PIN = 16

# To create extra 3.3V so each button has own power source
ALWAYS_HIGH_PIN = 40
#ALWAYS_HIGH_PIN2 = 38

CLOSE_MAGNET_SIGNAL = GPIO.LOW
OPEN_MAGNET_SIGNAL = GPIO.HIGH

LIGHT_ON = GPIO.HIGH
LIGHT_OFF = GPIO.LOW

RUNNING_PATH = 'Oven sound over one minute.mp3'
FINISH_PATH = 'oven ready sound.mp3'

class GPIOCommunicator(object):
    def __init__(self):
        GPIO.setmode(GPIO.BOARD)
        #GPIO.setmode(GPIO.BCM)
        GPIO.setup(PLAYER_BUTTON_PIN, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(OPERATOR_OVEN_OPEN_CLOSE_PIN, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(OPERATOR_ALLOW_START_PIN, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

        GPIO.setup(OVEN_OPEN_PIN, GPIO.OUT, initial=LIGHT_ON)
        GPIO.setup(OVEN_START_ALLOWED_OR_ON_PIN, GPIO.OUT, initial=LIGHT_OFF)

        GPIO.setup(ALWAYS_HIGH_PIN, GPIO.OUT, initial=GPIO.HIGH)
        #GPIO.setup(ALWAYS_HIGH_PIN2, GPIO.OUT, initial=GPIO.HIGH)

        GPIO.setup(MAGNET_LOCK_PIN, GPIO.OUT, initial=OPEN_MAGNET_SIGNAL)
        
        self.door_closed_time = None
        
    def set_button_callback(self, pin, callback):
        print("Setting callback for pin", pin);
        GPIO.add_event_detect(pin, GPIO.RISING, callback=callback, bouncetime=500)

    def set_button_callback_both(self, pin, callback):
        GPIO.add_event_detect(pin, GPIO.BOTH, callback=callback, bouncetime=400)
    
    def open_door(self):
        self.door_closed_time = None

        print("Unlocking magnet")
        GPIO.output(MAGNET_LOCK_PIN, OPEN_MAGNET_SIGNAL)

        print("Turning oven open light OFF")
        GPIO.output(OVEN_OPEN_PIN, LIGHT_ON)
        
    def close_door(self):
        self.door_closed_time = time.time()

        print("Turning oven open light OFF")
        GPIO.output(OVEN_OPEN_PIN, LIGHT_OFF)

        print("Locking magnet")
        GPIO.output(MAGNET_LOCK_PIN, CLOSE_MAGNET_SIGNAL)

    def allow_oven_start(self):
        print("Writing ON to 'oven permission' light")
        GPIO.output(OVEN_START_ALLOWED_OR_ON_PIN, LIGHT_ON)

    def revoke_permission_oven_start(self):
        print("Writing OFF to 'oven permission' light")
        GPIO.output(OVEN_START_ALLOWED_OR_ON_PIN, LIGHT_OFF)

    def get_door_closed_time(self):
        if self.door_closed_time:
            return time.time() - self.door_closed_time
        else:
            return 0

    def oven_closed(self):
        return self.get_door_closed_time() > 0

    def is_door_open(self):
        return GPIO.input(OPERATOR_ALLOW_START_PIN)


class Audioplayer(object):
    
    def __init__(self, running_path, finish_path):
        pygame.mixer.init()
        self.oven_running_path = running_path
        self.oven_finish_path = finish_path    
    
    def playsound(self, path, loop):
        pygame.mixer.music.load(path)
        
        if loop:
            pygame.mixer.music.play(-1)
        else:
            pygame.mixer.music.play()

        #while pygame.mixer.music.get_busy() == True:
        #    continue
        print("Finished playsound method")
    
    def play_oven_running(self):
        self.playsound(self.oven_running_path, loop=True)
        
    def play_oven_finish(self):
        self.playsound(self.oven_finish_path, loop=False)

class Oven(object):

    def __init__(self, gpioc, audioplayer):
        self.gpioc = gpioc
        self.audioplayer = audioplayer
        self.reset()

    def reset(self):
        self.allowed_to_start = False
        self.operator_button_closes_oven = True
        self.gpioc.revoke_permission_oven_start()
        self.reset_time = time.time()

    def close_oven(self, pin=None):

        if pin:
            print(f"Pin {pin} triggered")

        if not self.gpioc.oven_closed():
            print("Starting oven")
            self.gpioc.close_door()
            self.audioplayer.play_oven_running()
            
    def open_oven(self, pin=None):
        
        if pin:
            print(f"Pin {pin} triggered")

        if self.gpioc.oven_closed():
            print("Opening oven")
            self.gpioc.open_door()
            self.audioplayer.play_oven_finish()

        self.reset()

    def player_closes_oven(self, pin=None):

        if pin:
            print(f"Player pin ({pin}) triggered")

        if self.allowed_to_start:
            self.close_oven()
        else:
            print("Oven not allowed to close")

    def operator_open_or_close_oven(self, pin):
        
        if pin:
            print(f"Operator pin ({pin}) triggered")

        if self.operator_button_closes_oven:
            self.close_oven()
            self.gpioc.allow_oven_start()
            # toggle button
            self.operator_button_closes_oven = not self.operator_button_closes_oven
        else:
            self.open_oven()

        print(f"Operator button closes oven: {self.operator_button_closes_oven}" )

    def allow_oven_start(self, pin=None):

        if pin:
            print(f"Allow start pin ({pin}) triggered")

        time_passed_after_reset = time.time() - self.reset_time
        if time_passed_after_reset > WAIT_AFTER_RESET:
            self.allowed_to_start = True
            self.gpioc.allow_oven_start()
        else:
            print(f"Waiting for WAIT_AFTER_RESET to pass (time passed: {time_passed_after_reset}")

    def prevent_oven_start(self, pin=None):

        if pin:
            print(f"Allow start pin ({pin}) triggered")

        self.allowed_to_start = False
        self.gpioc.revoke_permission_oven_start()

    def door_open_or_closed(self, pin=None):

        if self.gpioc.is_door_open():
            self.prevent_oven_start()
        else:
            self.allow_oven_start()

gpio_communicator = GPIOCommunicator()
audioplayer = Audioplayer(RUNNING_PATH, FINISH_PATH)
oven = Oven(gpio_communicator, audioplayer)

gpio_communicator.set_button_callback(PLAYER_BUTTON_PIN, oven.player_closes_oven)
gpio_communicator.set_button_callback(OPERATOR_OVEN_OPEN_CLOSE_PIN, oven.operator_open_or_close_oven)
gpio_communicator.set_button_callback_both(OPERATOR_ALLOW_START_PIN, oven.door_open_or_closed)

#audioplayer.play_oven_running()

try:
    while True:
        time.sleep(.1)
        if gpio_communicator.get_door_closed_time() > TIME_CLOSED:
            oven.open_oven(None)
except KeyboardInterrupt:
    print("KeyboardInterrupt received, stopping and cleaning up GPIO")
    GPIO.cleanup()    
    
