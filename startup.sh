#!/bin/bash

# bash script to start oven code
# to have script start up on startup, add the following line to /etc/xdg/lxsession/LXDE-pi/autostart
# @sh /home/pi/rijks-oven/startup.sh 

cd /home/pi/rijks-oven/
lxterminal -e "python3 /home/pi/rijks-oven/run.py" &

cd /
